package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"strings"

	"code.sajari.com/docconv"
)

// Iterate through new-line separated string array until line == "Kills:"
// Then account for each successive line matching (number) (monster name): (Player Name) (kill weapon/method)
/*
	Campaign Log
	Session 1
	Day 1
	Stuff happens.
	Battle!
	Kills:
	1 goblin: John Dory rapier
	1 goblin: Dro’Namir magic stone
	+ 25 xp x2 from combat
	Day 2
	More stuff happens.
	Kills:
	2 giant spider: JD rapier
	1 giant spider: Dro’Namir magic stone
	+ 75 xp x2 from combat
*/

func getDocxText(filename string) string {
	_, err := os.Stat(filename)
	if err != nil {
		panic(err)
	}

	res, err := docconv.ConvertPath(filename)
	if err != nil {
		panic(err)
	}
	return res.Body
}

// Kill is a record of a specific kill event
type Kill struct {
	Number     int
	Monster    string
	KilledWith string
}

func aliases() map[string]string {
	aliases := make(map[string]string)
	aliases["JD"] = "John Dory"
	aliases["DN"] = "Dro’Namir"
	aliases["Dro'Namir"] = "Dro’Namir"
	aliases["Dro-Namir"] = "Dro’Namir"
	aliases["Iz"] = "Izella"
	aliases["R"] = "Rolen"
	return aliases
}

func monsterXPMap() map[string]int {
	monstersFile, err := os.Open("monsters.csv")
	if err != nil {
		fmt.Printf("Can't open monsters.csv file\n")
		panic(err)
	}
	monsterCSVReader := csv.NewReader(bufio.NewReader(monstersFile))
	records, err := monsterCSVReader.ReadAll()
	if err != nil {
		fmt.Printf("Can't parse monsters.csv file\n")
		panic(err)
	}
	monstersFile.Close()

	monsterXPMap := make(map[string]int)
	for _, line := range records {
		if line[0] != "" { //Do nothing for blank lines
			i, err := strconv.Atoi(line[1])
			if err != nil {
				fmt.Printf("Can't parse number to integer in line: %v\n", line)
				panic(err)
			}
			monsterXPMap[strings.ToLower(line[0])] = i
		}
	}
	return monsterXPMap
}

func printKills(kills map[string][]Kill) {
	totals := make(map[string]int)
	for charName := range kills {
		if charName != "Surrendered" {
			for i := range kills[charName] {
				plural := ""
				if kills[charName][i].Number != 1 {
					plural = "s"
				}
				fmt.Printf("%s %s %v %s%s %s %s\n", charName, "killed", kills[charName][i].Number, kills[charName][i].Monster, plural, "with", kills[charName][i].KilledWith)
				totals[charName] += kills[charName][i].Number
			}
		}
	}
	surrenderedKey := "Surrendered"
	if surrendered, ok := kills[surrenderedKey]; ok {
		for i := range surrendered {
			plural := ""
			if surrendered[i].Number != 1 {
				plural = "s"
			}
			fmt.Printf("%v %s%s %s\n", surrendered[i].Number, surrendered[i].Monster, plural, "surrendered")
			totals[surrenderedKey] += surrendered[i].Number
		}
	}
	fmt.Println()
	fmt.Println("Total kills:")
	for charName := range totals {
		fmt.Printf("%s: %v\n", charName, totals[charName])
	}
}

func calcTotalXPFromKills(kills map[string][]Kill, monsterXPMap map[string]int) map[string]int {
	xpFromKills := make(map[string]int)
	for charName := range kills {
		for i := range kills[charName] {
			monsterName := strings.ToLower(kills[charName][i].Monster)
			for j := 0; j < kills[charName][i].Number; j++ {
				xpFromKills[charName] += monsterXPMap[monsterName]
			}
			if monsterXPMap[monsterName] == 0 {
				fmt.Printf("No monster entry for %v\n", kills[charName][i].Monster)
			}
		}
	}
	return xpFromKills
}

func printXPFromKills(xpFromKills map[string]int) {
	fmt.Println("XP from kills:")
	for charName := range xpFromKills {
		fmt.Printf("%s: %v xp\n", charName, xpFromKills[charName])
	}
}

func printTotalXPFromKills(xpFromKills map[string]int) {
	totalXP := 0
	for charName := range xpFromKills {
		totalXP += xpFromKills[charName]
	}
	fmt.Printf("Total XP from kills: %v\n", totalXP)
}

func printStringSet(stringMap map[string]bool) {
	for s := range stringMap {
		fmt.Println(s)
	}
}

func main() {
	aliases := aliases()
	kills := make(map[string][]Kill)
	monsters := make(map[string]bool)
	killedWithTypes := make(map[string]bool)

	filename := "test document.docx"
	verbose := ""
	if len(os.Args) > 1 {
		filename = os.Args[1]
	}
	if len(os.Args) > 2 {
		verbose = os.Args[2]
	}

	fulltext := getDocxText(filename)

	recordingMode := false
	for i, line := range strings.Split(fulltext, "\n") {
		if recordingMode {
			if len(line) > 0 { //If the line is empty, I probably made a mistake; skip this line
				if line[0] == '+' { //If the start of the line is a '+', stop recording kills; this is the xp tally at the end of the block
					recordingMode = false
				} else { //Record a kill. Key variables are number, monster, charName and killedWith
					// Split line at colon. Format is as follows: "1 monster: Char Name killed with weapon"
					halfline := strings.Split(line, ":")
					// First halfline contains number and monster
					halfline_split_1 := strings.Split(halfline[0], " ")
					// number is the first item and is numeric
					number, err := strconv.Atoi(halfline_split_1[0])
					if err != nil {
						fmt.Printf("%s %v%s%s%s", "Integer parse error at start of line", i, ": '", line, "'")
						panic(err)
					}
					// monster is all other words
					monster := strings.Join(halfline_split_1[1:], " ")
					// Trim off last word if it's in brackets
					monsterTrimmed := monster
					monsterSplit := strings.Split(monster, " ")
					if strings.Contains(monsterSplit[len(monsterSplit)-1], "(") {
						monsterTrimmed = strings.Join(monsterSplit[0:len(monsterSplit)-1], " ")
					}
					// Second halfline contains charName and killedWith
					halfline_split_2 := strings.Split(halfline[1], " ")
					// charName is all words starting from the first that start with a capital letter
					lastNameIndex := 0
					nameIndex := 0
					for j, word := range halfline_split_2 {
						if word != "" {
							firstLetter := strings.Split(word, "")[0]
							if firstLetter != strings.ToUpper(firstLetter) {
								nameIndex = j
								break
							} else {
								lastNameIndex = j
							}
						}
					}
					lastNameIndex++
					if len(halfline_split_2[1:lastNameIndex]) < 1 {
						fmt.Printf("%v is not long enough. You must be missing someone's name or signifier.\n", halfline_split_2)
						return
					}
					charName := strings.Join(halfline_split_2[1:lastNameIndex], " ")
					// Sometimes I use aliases to refer to character names so this helps there
					aliasCharName := charName
					for alias := range aliases {
						if charName == alias {
							aliasCharName = aliases[alias]
						}
					}
					// killedWith is all other words
					var killedWith string
					var killedWithTrimmed string
					if nameIndex == 0 {
						killedWith = "null"
					} else {
						killedWith = strings.Join(halfline_split_2[nameIndex:], " ")
						// Trim off last word if it's 'crit' or 'smite' or 'sneak attack'
						killedWithTrimmed = killedWith
						killedWithSplit := strings.Split(killedWith, " ")
						//TODO: this might break if weapon string is too short?
						if killedWithSplit[len(killedWithSplit)-1] == "smite" {
							killedWithSplitReduced := killedWithSplit[0 : len(killedWithSplit)-1]
							killedWithTrimmed = strings.Join(killedWithSplitReduced, " ")
						}
						if len(killedWithSplit) > 1 && killedWithSplit[len(killedWithSplit)-2] == "sneak" && killedWithSplit[len(killedWithSplit)-1] == "attack" {
							killedWithSplitReduced := killedWithSplit[0 : len(killedWithSplit)-2]
							killedWithTrimmed = strings.Join(killedWithSplitReduced, " ")
						}
						if killedWithSplit[len(killedWithSplit)-1] == "crit" {
							killedWithSplitReduced := killedWithSplit[0 : len(killedWithSplit)-1]
							killedWithTrimmed = strings.Join(killedWithSplitReduced, " ")
						}
					}
					
					// Add details to kills log
					kills[aliasCharName] = append(kills[aliasCharName], Kill{Number: number, Monster: monsterTrimmed, KilledWith: killedWith})
					monsters[monsterTrimmed] = true
					killedWithTypes[killedWithTrimmed] = true
				}
			}
		} else {
			if line == "Kills:" { //Start recording kills; following lines will contain kill info
				recordingMode = true
			}
		}
	}

	fmt.Println()
	printKills(kills)
	fmt.Println()
	totalXPFromKills := calcTotalXPFromKills(kills, monsterXPMap())
	printXPFromKills(totalXPFromKills)
	fmt.Println()
	printTotalXPFromKills(totalXPFromKills)
	if verbose == "-v" {
		fmt.Println()
		printStringSet(monsters)
		fmt.Println()
		printStringSet(killedWithTypes)
	} else if verbose != "" {
		fmt.Println()
		fmt.Println(verbose + "is not a valid option. Valid options are '-v' for verbose.")
	}
}
