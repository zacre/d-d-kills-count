# README #

This parses a provided Word document (.docx) and searches for sections in the following format, and collates kills from multiple entries to provide data:

Kills:  
1 creature: Person Name kill method  
2 creature: Other Person kill method  
\+ Y xp from combat
